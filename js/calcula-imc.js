var titulo = document.querySelector("h1");
titulo.textContent = "Alessandro JS";


var pacientes = document.querySelectorAll(".paciente");

for (var i = 0; i < pacientes.length; i++) {
    var paciente = pacientes[i];
    var pNome = paciente.querySelector(".info-nome");
    var pPeso = paciente.querySelector(".info-peso");
    var pAltura = paciente.querySelector(".info-altura");
    var pGordura = paciente.querySelector(".info-gordura");
    var pImc = paciente.querySelector(".info-imc");

    var peso = pPeso.textContent;
    var altura = pAltura.textContent;
    var nome = pNome.textContent;
    var gordura = pGordura.textContent;

    var pesoReal = calculaPeso(peso);
    var alturaReal = calculaAltura(altura);



    if (!pesoReal) {
        peso = false;
        pImc.textContent = "Peso inválido!";
        paciente.classList.add("conteudo-invalido");
    }

    if (!alturaReal) {
        altura = false;
        pImc.textContent = "Altura inválida!";
        paciente.classList.add("conteudo-invalido");
    }

    if (peso && altura) {
        var pCalculo = calculaImc(peso, altura);
        pImc.textContent = pCalculo;
    }
}
function calculaImc(peso, altura) {
    //var imc = 0;
    var imc = peso / (altura * altura);
    return imc.toFixed(2);
}

function calculaPeso(peso) {
    if (peso > 0 && peso < 700) {
        return true
    } else {
        return false
    }
}
function calculaAltura(altura) {
    if (altura > 0 && altura <= 2.8) {
        return true
    } else {
        return false
    }
}


