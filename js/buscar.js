var buscar = document.querySelector("#buscar-paciente");

buscar.addEventListener("click", function () {

    var xhr = new XMLHttpRequest();
    xhr.open("GET", "https://api-pacientes.herokuapp.com/pacientes");
    var erroAjax = document.querySelector("#erro-ajax");


    xhr.addEventListener("load", function () {
        if (xhr.status == 200) {
            erroAjax.classList.add("invisivel");
            var pega = xhr.responseText;
            var converte = JSON.parse(pega);

            converte.forEach(function (paciente) {
                adicionaPacienteNaTabela(paciente);
            })


        } else {
            erroAjax.classList.remove("invisivel");
            console.log(xhr.status);
            console.log(xhr.responseText);
        }
    })


    xhr.send();

});