var filtro = document.querySelector("#filtro");

filtro.addEventListener("input", function () {
    console.log(this.value);

    var paciente = document.querySelectorAll(".paciente");

    if (this.value.length > 0) {
        for (var x = 0; x < paciente.length; x++) {
            var pacientes = paciente[x];
            var tdNome = pacientes.querySelector(".info-nome");
            var nome = tdNome.textContent;
            var expressao = new RegExp(this.value, "i");

            if (!expressao.test(nome)) {
                pacientes.classList.add("invisivel");
            } else {
                pacientes.classList.remove("invisivel")
            }
        }
    } else {
        for (var x = 0; x < paciente.length; x++) {
            var pacientes = paciente[x];
            pacientes.classList.remove("invisivel")
        }
    }

})