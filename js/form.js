var botao = document.querySelector("#adicionar-paciente");
botao.addEventListener('click', function (event) {

    event.preventDefault();

    var form = document.querySelector("#formulario");

    var pessoaFinal = pessoa(form);

    var erro = pacienteTeste(pessoaFinal);

    adicionaPacienteNaTabela(paciente);

    if (erro.length > 0) {
        ativarMensagem(erro);
        return;
    }

    var colocaTr = adicionoTr(pessoaFinal);

    var tabela = document.querySelector("#tabela-pacientes");
    tabela.appendChild(colocaTr);

    var limpaErro = document.querySelector("#mensagem");
    limpaErro.innerHTML = "";

    form.reset();


})

function adicionaPacienteNaTabela(paciente){

    var colocaTr = adicionoTr(paciente);
    var tabela = document.querySelector("#tabela-pacientes");
    tabela.appendChild(colocaTr);

}

function pessoa(form) {
    var pessoa = {
        nome: form.nome.value,
        peso: form.peso.value,
        altura: form.altura.value,
        gordura: form.gordura.value,
        imc: calculaImc(form.peso.value, form.altura.value)
    }
    return pessoa;
}

function adicionoTr(pessoaFinal) {
    var pacienteTr = document.createElement("tr");

    pacienteTr.appendChild(montarTd(pessoaFinal.nome, ".info-nome"));
    pacienteTr.appendChild(montarTd(pessoaFinal.peso, ".info-peso"));
    pacienteTr.appendChild(montarTd(pessoaFinal.altura, ".info-altura"));
    pacienteTr.appendChild(montarTd(pessoaFinal.gordura, ".info-gordura"));
    pacienteTr.appendChild(montarTd(pessoaFinal.imc, ".info-imc"));

    return pacienteTr;
}

function montarTd(dados, classe) {
    var td = document.createElement("td");
    td.textContent = dados;
    td.classList.add(classe);
    return td;
}
function pacienteTeste(pessoa) {

    var erros = [];

    if (!calculaPeso(pessoa.peso)) {
        erros.push("Peso Inválido")
    }

    if (!calculaAltura(pessoa.altura)) {
        erros.push("Altura Inválida")
    }

    if (pessoa.peso == "") {
        erros.push("O peso não pode ser em branco")
    }

    if (pessoa.altura == "") {
        erros.push("A altura não pode ser em branco")
    }

    if (pessoa.gordura == "") {
        erros.push("A gordura não pode ser em branco")
    }

    if (pessoa.nome == "") {
        erros.push("O nome não pode ser em branco")
    }

    return erros;
}

function ativarMensagem(erro) {

    var mensagem = document.querySelector("#mensagem");
    mensagem.innerHTML = "";
    erro.forEach(function (erros) {
        var erroLi = document.createElement("li");
        erroLi.textContent = erros;
        mensagem.appendChild(erroLi);
    })


}